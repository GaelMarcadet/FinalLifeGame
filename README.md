# Exemples d'utilisation de l'application JeuDeLaVie

| Utilisation                     |Commande| Exemple |
|---------------------------------|--------|----------|
| Noms                            |java -jar nom_jar.jar -name|`java -jar jdlv.jar -name`|
| Aide                            |java -jar nom_jar.jar -h|`java -jar jdlv.jar -h`|
| Simulation sur 1 fichier        |java -jar nom_java.jar -s -type max fichier|`java -jar jdlv.jar -s -o 5 /Users/gael/IdeaProjects/jdlv/lifep/ACORN.LIF`         |
| Type d'évolution sur un fichier |java -jar nom_java.jar -c -type max fichier|`java -jar jdlv.jar -c -o 5 /Users/gael/IdeaProjects/jdlv/lifep/ACORN.LIF`          |
| Type d'évolution sur un dossier |java -jar nom_java.jar -w -type max dossier| `java -jar jdlv.jar -w -o 5 /Users/gael/IdeaProjects/jdlv/lifep`         |