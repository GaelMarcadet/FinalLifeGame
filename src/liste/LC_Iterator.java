package liste;

import java.util.Iterator;
import java.util.NoSuchElementException;

/***
 * Un itérateur sur une collection de type liste permet d'utiliser un et élément de type précisé.
 * @param <T> Le type utilisé dans l'implémentation de cette liste chainée.
 * 
 * @author Groupe 3
 */
public class LC_Iterator<T> implements Iterator {

    private Maillon<T> maillonIndex;

    /***
     * Construit un nouvel itérateur, qui se place au début de l'objet appelant. Plus précisement, il pointe sur la tete de la liste qui l'utilise.
     * @param tete Un maillon correspondant à la tête
     */
    public LC_Iterator(Maillon<T> tete) {
        maillonIndex = tete;
    }

    /***
     * Retourne true si l'itération à plus d'éléments. Plus techniquement, retourne true si {@link #next()} retourne un élément plutôt qu'il lève une erreur.
     * @return True si l'itération a au moins un élément suivant d'éléments.
     */
    @Override
    public boolean hasNext() {
        return maillonIndex != null;
    }

    /***
     * Retourne l'élément suivant dans l'itération.
     * @return l'élément suivant dans l'itération.
     * @throws NoSuchElementException - si l'itération n'a plus d'élément.
     */
    @Override
    public Comparable<T> next() throws NoSuchElementException{
        if (maillonIndex == null) throw new NoSuchElementException();
        Comparable<T> res = maillonIndex.valeur;
        maillonIndex = maillonIndex.getSuivant();
        return res;
    }

    /**
     * Cette methode permet d'ajouter directement en tete de la liste. <strong>ATTENTION</strong>, elle est a manipuler avec beaucoup de precaution. En effet, il faut s'assurer que l'utiliser ne derangera pas l'ordre naturel etablit pour ces elements.
     * @param v
     *        l'element a ajouter en tete
     */
    public void ajouterAvant(Comparable<T> v){
        maillonIndex.suivant = new Maillon(maillonIndex.valeur,maillonIndex.suivant);
        maillonIndex.valeur = v;
        maillonIndex = maillonIndex.suivant;
    }
    
    /**
     * Cette methode permet d'ajouter un element a la suite de l'element indexe . <strong>ATTENTION</strong>, elle est a manipuler avec beaucoup de precaution. En effet, il faut s'assurer que l'utiliser ne derangera pas l'ordre naturel etablit pour ces elements.
     * @param v
     *        l'element a la suite de l'element indexe
     */
    public void ajouterApres(Comparable<T> v){
        maillonIndex.suivant = new Maillon(v,maillonIndex.suivant);
    }

    /**
     * Retourne true si l'itération n'a plus d'éléments. Plus techniquement, retourne true si {@link #next()} lève une erreur.
     * @return True si l'itération n'a aucun élément suivant.
     */
    public boolean suivantNull(){
        return maillonIndex.suivant == null;
    }

    /**
     * Retourne l'element contenu dans le maillon indexe, sans modifier ce dernier
     * @return l'element contenu dans le maillon indexé
     */
    public Comparable<T> getValeur(){
        return maillonIndex.valeur;
    }

    /**
     * Decale le maillon indexe sur son suivant dans la liste. Le maillon index est alors le suivant de l'ancien index.
     */
    public void suivant(){
        maillonIndex = maillonIndex.suivant;
    }


}
