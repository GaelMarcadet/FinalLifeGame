package liste;

public class Maillon<T> {
    public Comparable<T> valeur;
    public Maillon suivant;

    /***
     * Construit un nouveau maillon, qui encapsule l'element "valeur" et ne cree pas de lien.
     * @param valeur
     *        element a encapsuler, contenu dans le maillon.
     */
    public Maillon(Comparable<T> valeur) {
        this(valeur, null);
    }

    /***
     * Construit un nouveau maillon, qui encapsule l'element "valeur" et le lie a  suivant "suivant".
     * @param valeur
     *        element a encapsuler, contenu dans le maillon.
     * @param m
     *        maillon lie a l'element passe en parametre
     */
    public Maillon(Comparable<T> valeur, Maillon m) {
        this.valeur=valeur;
        suivant=m;
    }

    /***
     * Retourne l'element lie au maillon appelant.
     * @return l'element lie au maillon appelant.
     */
    public Maillon getSuivant() {
        return suivant;
    }

    /**
     * Retourne la valeur contenue dans l'element appelant.
     * @return la valeur contenue dans l'element appelant.
     */
    public Comparable<T> getValeur() {
        return valeur;
    }

    /**
     * Retourne une representation de l'ensemble sous forme de chaîne de caractère.
     * La valeur contenu dans le maillon est entouré de crochets "[]".
     *
     * @return une representation de l'ensemble sous forme de chaîne de caractère.
     */
    @Override
    public String toString() {
        return "["+valeur+"]";
    }

    /**
     * Initialise le maillon suivant de l'element appelant avec le parametre donne.
     * @param newSuivant
     *        le nouveau maillon lie a l'element appelant.
     */
    public void setSuivant(Maillon<T> newSuivant) {
        suivant = newSuivant ;
    }
}
