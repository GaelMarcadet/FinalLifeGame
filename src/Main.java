import generateur.Generateur;
import generation.*;
import gestionJeu.FichierGeneration;
import gestionJeu.GestionDossierGeneration;
import gestionJeu.JeuGeneration;
import generateur.*;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Nombre d'options passe en parametre incorrect.");
            System.exit(1);
        }
        else {
            int duree;
            String nom;
            String type;
            JeuGeneration g_init = null;
            FichierGeneration f;
            switch (args[0]) {
                case "-name":
                    System.out.println("ADAM Daniel\nMARCADET Gaël\nQUERAULT Mathis\nTRIVALEU Manon");
                    break;

                case "-h":
                    System.out.println("-name : affiche les noms et prénoms des étudiants.\n" +
                            "\n" +
                            "-h : rappelle la liste des options du programme.\n\n" +
                            "-s -o/b/f d fichier.lif : exécute la simulation du jeu contenu dans le fichier fichier.lif en suivant les règles de la configuration ouverte (-o), bouclee (-b) ou fermée (-f) pendant une duree d.\n\n" +
                            "-c -o/b/f max fichier.lif : calcule le type d'évolution du jeu contenu dans le fichier.lif avec ses caracteristiques en suivant les règles de la configuration ouvert (-o), bouclee (-b) ou fermee (-f). Le calcul des caracteristiques se fait au plus sur d generations." +
                            "-w -o/b/f max dossier : calcule le type d'évolution et les caracteristiques de tous les jeux contenus dans le dossier en suivant la configuration ouverte (-o), bouclee (-b) ou fermee (-f). L'ensemble des resultats est ecrit dans un fichier html.");
                    break;

                case "-w" :
                    if (args.length!=4) {
                        System.out.println("Erreur dans les paramètres. Pour plus d'informations, relancez le programme avecc la commande -h.");
                        System.exit(1);
                    }
                    else {
                        try {
                            duree = Integer.parseInt(args[2]);
                            nom = args[3];
                            Generateur gen = null;
                            switch (args[1]) {
                                case "-f" :
                                    gen = GenerationFactory.generateurGenerationFermee();
                                    break;
                                case "-o" :
                                    gen = GenerationFactory.generateurGenerationOuverte();
                                    break;
                                case "-b" :
                                    gen = GenerationFactory.generateurGenerationBouclee();
                                    break;
                                default :
                                    System.out.println("Erreur dans les paramètres! Pour plus d'informations, relancez le programme avec la commande -h.");
                                    System.exit(1);
                                    break;
                            }
                            GestionDossierGeneration d = new GestionDossierGeneration(duree, gen);
                            d.chargerDossier(nom);
                            d.chargerDossierHTML();
                        } catch(NumberFormatException e) {
                            System.out.println("Erreur dans les paramètres! La durée doit être un chiffre! Pour plus d'informations, relancez le programme avec la commande -h");
                        } catch (IOException e) {
                            System.out.println("Problème dans le chargement des fichiers ou du dossier.");
                            System.exit(1);
                        }
                    }

                default :
                    if (args.length!=4) System.out.println("Erreur dans les paramètres. Pour plus d'informations, relancez le programme avec la commande -h.");
                    else {
                        try {
                            duree = Integer.parseInt(args[2]);
                            if (duree<=0) {
                                System.out.println("Durée impossible! Pour plus d'informations, relancez le programme avec la commande -h.");
                                System.exit(1);
                            }

                            nom = args[3];
                            switch (args[1]) {
                                case "-f" :
                                    g_init = new GenerationFermee();
                                    break;
                                case "-o" :
                                    g_init = new GenerationOuverte();
                                    break;
                                case "-b" :
                                    g_init = new GenerationBouclee();
                                    break;
                                default :
                                    System.out.println("Erreur dans les paramètres! Pour plus d'informations, relancez le programme avec la commande -h.");
                                    System.exit(1);
                            }
                            try {
                                switch (args[0]) {
                                    case "-s":
                                        f = new FichierGeneration(nom, g_init, duree);
                                        f.simulation();
                                        break;
                                    case "-c":
                                        f = new FichierGeneration(nom, g_init, duree);
                                        int[] tab = f.getGeneration_initiale().typeEvolution(duree);
                                        System.out.println("Ce fichier a pour evolution : " + GestionDossierGeneration.getType(tab[0]) + "\nTaille de la queue : " + String.valueOf(tab[1]) + "\nTaille de la boucle : " + String.valueOf(tab[2]));
                                        break;
                                }
                            }catch (Exception e){}
                        } catch (NumberFormatException e) {
                            System.out.println("Paramètres incorrects! La durée doit être un chiffre. Pour plus d'informations, relancez le programme avec la commande -h.");
                        }
                    }
                    break;
            }
        }
    }
}