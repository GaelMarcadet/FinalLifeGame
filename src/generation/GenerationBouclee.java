package generation;

import liste.ListeChainee;

/**
 * La classe GenerationBouclee représente une generation bouclee.
 * Une generation bouclee est une representation du jeu de la vie avec une limite de taille.
 * Une cellule au bord de la generation, aura pour voisines les cellules a proximite mais aussi celles
 * de l'autre cote de la generation.
 *
 * @author Groupe 3
 * @see Generation
 */
public class GenerationBouclee extends Generation {

    /**
     * La methode getInstance retourne une instance de generation bouclee.
     *
     * @return GenerationBouclee retourne une nouvelle generation bouclee.
     */
    @Override
    Generation getInstance() {
        return new GenerationBouclee();
    }

    /**
     * La methode estValide verifie si une cellule est valide or une generation bouclee n'impose pas de condition a
     * une cellule pour etre valide. Dans cette configuration du jeu de la vie cette methode retoune toujours vrai
     *
     * @param c
     *        est une cellule dont on veut savoir si elle est valide.
     * @return Boolean {@code true}
     */
    @Override
    boolean estValide(Cellule c) {
        return true;
    }

    /**
     * La methode getCellule retourne une cellule en parametre.
     *
     * @param c
     *        est une cellule dont on veut obtenir la version correcte pour la generation.
     * @return Cellule c est une cellule correcte.
     */
    @Override
    Cellule getCellule(Cellule c) {
        int x = c.getX(), y = c.getY();
        if (x < min_x) x = max_x;
        if (x > max_x) x = min_x;
        if (y < min_y) y = max_y;
        if (y > max_y) y = min_y;
        return new Cellule(x, y, c.getVoisins());
    }

    /**
     * La methode getVoisines retourne les voisines d'une cellule tout en prenant en compte les caracteristiques d'une
     * generation bouclee.
     *
     * @return ListeChainee<Cellule> res est une liste des voisines de la cellule {@code cellule}.
     */
    public ListeChainee<Cellule> getVoisines(int i, int j) {
        ListeChainee<Cellule> res = new ListeChainee<>((c1, c2) -> -c1.compareTo(c2));
        if (i == 0 && j == 0) {
            for (Cellule cell : lesCellules) {
                Cellule c = new Cellule(cell.getX() + i, cell.getY() + j, 10);
                res.ajouter(getCellule(c));
            }
        } else {
            for (Cellule cell : lesCellules) {
                Cellule c = new Cellule(cell.getX() + i, cell.getY() + j, 1);
                res.ajouter(getCellule(c));
            }
        }
        return res;
    }

}