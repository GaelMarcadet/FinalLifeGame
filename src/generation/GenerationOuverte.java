package generation;

import liste.ListeChainee;

/**
 * La classe GenerationOuverte represente une generation ouverte.
 * Une generation est une representation du jeu de la vie sans limite de taille.
 *
 * @author Groupe 3
 * @see Generation
 */
public class GenerationOuverte extends Generation {


    /**
     * La methode getInstance retourne une instance de generation ouverte.
     *
     * @return GenerationOuverte retourne une nouvelle generation ouverte.
     */
    @Override
    Generation getInstance() {
        return new GenerationOuverte();
    }

    /**
     * La methode estValide verifie si une cellule est valide or une generation ouverte n'impose pas de condition a
     * une cellule pour etre valide. Dans cette configuration du jeu de la vie cette methode retoune toujours vrai
     *
     * @param c est une cellule dont on veut savoir si elle est valide.
     * @return Boolean {@code true}
     */
    @Override
    boolean estValide(Cellule c) {
        return true;
    }

    /**
     * La methode getCellule retourne une cellule en parametre.
     *
     * @param c est une cellule dont on veut obtenir la version correcte pour la generation.
     * @return Cellule c est une cellule correcte.
     */
    @Override
    Cellule getCellule(Cellule c) {
        return c;
    }

    /**
     * La methode traitementAffichage retourne une generation
     * @param g est une generation
     * @param cellules represente un ensemble de cellules
     * @return Generation g une generation
     */
    protected Generation traitementAffichage(Generation g, ListeChainee<Cellule> cellules) {
        ListeChainee<Cellule> pointeurListe = g.lesCellules;
        int mn_x = 0, mx_x = 0, mn_y = 0, mx_y = 0;
        for (Cellule c : cellules) {
            int nb_voisins = c.getVoisins();
            if (nb_voisins >= 10) {
                nb_voisins = nb_voisins % 10;
                if (nb_voisins == 2 || nb_voisins == 3) {
                    int x = c.getX(), y = c.getY();
                    if (x < mn_x) mn_x = x;
                    if (x > mx_x) mx_x = x;
                    if (y < mn_y) mn_y = y;
                    if (y > mx_y) mx_y = y;
                    pointeurListe.ajouterPremier(c);
                }
            } else if (nb_voisins == 3) {
                pointeurListe.ajouterPremier(c);
                int x = c.getX(), y = c.getY();
                if (x < mn_x) mn_x = x;
                if (x > mx_x) mx_x = x;
                if (y < mn_y) mn_y = y;
                if (y > mx_y) mx_y = y;

            }

        }
        g.min_x = mn_x;
        g.max_x = mx_x;
        g.min_y = mn_y;
        g.max_y = mx_y;
        return g;

    }
}